package com.app.plenitude7;

import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import classes.java.Connect;
import classes.fragments.FragAccueil;
import classes.fragments.FragBrowser;
import classes.fragments.FragListeApps;
import classes.fragments.FragNewCompte;

public class Main extends AppCompatActivity {
    private FragAccueil accueil;
    private FragListeApps liste;
    private FragNewCompte newCompte;
    private FragBrowser browser;
    private Connect connect;
    private SharedPreferences pref;

    public Main(){
        liste = new FragListeApps();
        newCompte = new FragNewCompte();
        browser = new FragBrowser();
        connect = new Connect(this);
        accueil = new FragAccueil();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getFragmentManager();
        if(fragmentManager.getBackStackEntryCount() == 0) finish();
        else fragmentManager.popBackStackImmediate();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = getPreferences(Context.MODE_PRIVATE);
        setContentView(R.layout.activity_main);
        getFragmentManager().beginTransaction().add(R.id.mainLayout, accueil).commit();
    }

    public void showListe(View view){
        getFragmentManager().beginTransaction().replace(R.id.mainLayout, liste).addToBackStack(null).commit();
    }

    public void showAccueil(View view){
        getFragmentManager().beginTransaction().replace(R.id.mainLayout, accueil).addToBackStack(null).commit();
    }

    public void showNew(View view){
        getFragmentManager().beginTransaction().replace(R.id.mainLayout, newCompte).addToBackStack(null).commit();
    }

    public void showBrowser(View view){
        getFragmentManager().beginTransaction().replace(R.id.mainLayout, browser).addToBackStack(null).commit();
    }


    public FragBrowser getBrowser(){return browser;}
    public FragListeApps getListe(){return liste;}
    public FragAccueil getAccueil(){return accueil;}
    public SharedPreferences getPref(){return pref;}
    public Connect getConnect(){
        return connect;
    }
}
