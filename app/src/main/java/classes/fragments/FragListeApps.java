package classes.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.app.plenitude7.Main;
import com.app.plenitude7.R;

import java.util.ArrayList;

import classes.java.Tools;
import classes.java.Tuple;

/**
 * Created by leuks on 18/03/2016.
 */
public class FragListeApps extends Fragment {
    public static final String CONCENTRATION = "AS0775-006";
    public static final String ANTI_STRESS = "AS0231-002";
    public static final String RELAXATION = "AS0342-003";
    public static final String MEDITATION = "MP0453-004";
    public static final String SOMMEIl = "SR0664-005";
    public static final String TRAINING  = "ATS0120-001";
    public static final String CONFIANCE  = "CS2132-009";
    public static final String TIMIDITE  = "TM3592-010";
    public static final String DEPRIME  = "AN0836-007";
    public static final String ANXIETE  = "AN0785-006";
    public static final String OBESITE  = "OS1847-008";
    public static final String TABAC  = "TA5865-011";
    public static final String ALCOOL  = "AA6317-012";
    public static final String RUPTURE  = "RS4183-011";

    private Main main;
    private ArrayList<Tuple<String, Integer>> data;

    public FragListeApps(){
        data = new ArrayList<Tuple<String, Integer>>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View thisView = inflater.inflate(R.layout.liste, container, false);
        ListView liste = (ListView) thisView.findViewById(R.id.liste);
        TextView listHeader = (TextView) thisView.findViewById(R.id.listHeader);
        this.main = ((Main) getActivity());
        main.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);

        //header
        Typeface font = Typeface.createFromAsset(main.getAssets(), "fonts/SairaExtraCondensed-Light.ttf");
        listHeader.setTypeface(font);

        //list
        ListAdapter adapter = new ListAdapter(getActivity().getBaseContext(), data);
        liste.setAdapter(adapter);
        liste.setOnItemClickListener(adapter);
        return thisView;
    }

    public void addData(String cursus){
        if(exists(cursus)) return;

        switch(cursus){
           /* case CONCENTRATION:
                data.add(new Tuple<String, Integer>(CONCENTRATION, R.drawable.concentration));
                break;*/
            case ANTI_STRESS:
                data.add(new Tuple<String, Integer>(ANTI_STRESS, R.drawable.antistress));
                break;
            case MEDITATION:
                data.add(new Tuple<String, Integer>(MEDITATION, R.drawable.meditation));
                break;
            case RELAXATION:
                data.add(new Tuple<String, Integer>(RELAXATION, R.drawable.relaxation));
                break;
            /*case TRAINING:
                data.add(new Tuple<String, Integer>(TRAINING, R.drawable.training));
                break;*/
            case SOMMEIl:
                data.add(new Tuple<String, Integer>(SOMMEIl, R.drawable.sommeil));
                break;
            case CONFIANCE:
                data.add(new Tuple<String, Integer>(CONFIANCE, R.drawable.confiance));
                break;
            case TIMIDITE:
                data.add(new Tuple<String, Integer>(TIMIDITE, R.drawable.timidite));
                break;
            case DEPRIME:
                data.add(new Tuple<String, Integer>(DEPRIME, R.drawable.deprime_depression));
                break;
            case ANXIETE:
                data.add(new Tuple<String, Integer>(ANXIETE, R.drawable.anxiete_angoisse));
                break;
            case OBESITE:
                data.add(new Tuple<String, Integer>(OBESITE, R.drawable.obesite_surpoids));
                break;
            case TABAC:
                data.add(new Tuple<String, Integer>(TABAC, R.drawable.tabac));
                break;
            case ALCOOL:
                data.add(new Tuple<String, Integer>(ALCOOL, R.drawable.alcool));
                break;
            case RUPTURE:
                data.add(new Tuple<String, Integer>(RUPTURE, R.drawable.separation_rupture));
                break;
        }
    }

    public boolean exists(String product){
        for(Tuple<String, Integer> t : data){
            if(t.getArg1().equals(product)) return true;
        }
        return false;
    }

    private class ListAdapter extends BaseAdapter implements OnItemClickListener {
        private ArrayList<Tuple<String, Integer>> tuples;
        private Context context;

        public ListAdapter(Context context, ArrayList tuples) {
            this.tuples = tuples;
            this.context = context;
        }

        @Override
        public int getCount() {
            return tuples.size();
        }

        @Override
        public Object getItem(int i) {
            return tuples.get(i);
        }

        @Override
        public long getItemId(int i) {
            return tuples.indexOf(getItem(i));
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            MyViewHolder mViewHolder = null;

            if (convertView == null) {
                LayoutInflater mInflater = (LayoutInflater) context
                        .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

                convertView = mInflater.inflate(R.layout.element, parent, false);

                mViewHolder = new MyViewHolder();

                mViewHolder.imageView = (ImageView) convertView
                        .findViewById(R.id.imageElem);

                convertView.setTag(mViewHolder);
            } else {
                mViewHolder = (MyViewHolder) convertView.getTag();
            }

            Tuple tuple = (Tuple) getItem(position);

            //mViewHolder.imageView.setImageResource((int) tuple.getArg2());



            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(getResources(), (int) tuple.getArg2(), options);

            // Calculate inSampleSize
            options.inSampleSize = Tools.calculateInSampleSize(options, 300, 600);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            Bitmap b = BitmapFactory.decodeResource(mViewHolder.imageView.getResources(), (int) tuple.getArg2(), options);
            mViewHolder.imageView.setImageBitmap(b);


            return convertView;
        }


        private class MyViewHolder {
            ImageView imageView;
        }


        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            System.out.println(main);
            main.getBrowser().setChoice(this.tuples.get(position).getArg1());
            main.showBrowser(view);
        }

    }
}
