package classes.fragments;

import android.app.Fragment;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.plenitude7.Main;
import com.app.plenitude7.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import classes.java.Connect;

/**
 * Created by leuks on 18/03/2016.
 */
public class FragAccueil extends Fragment {
    private String preLogin;
    private Drawable baseBackground;

    public FragAccueil(){
        super();
        this.preLogin=null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.accueil,container, false);
        final Main main = ((Main)getActivity());
        final Connect connect = ((Main) getActivity()).getConnect();
        Button buttonEng = (Button) thisView.findViewById(R.id.buttonNew);
        Button buttonCo = (Button) thisView.findViewById(R.id.buttonCo);
        EditText emailField = (EditText) thisView.findViewById(R.id.champLogin);
        EditText passwdField = (EditText) thisView.findViewById(R.id.champMdp);
        Typeface fontButton = Typeface.createFromAsset(main.getAssets(), "fonts/SairaExtraCondensed-Light.ttf");
        Typeface fontField = Typeface.createFromAsset(main.getAssets(), "fonts/Jura-DemiBold.ttf");
        main.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Auth
        final TextView textLogin = (TextView) thisView.findViewById(R.id.champLogin);
        final TextView textMdp = (TextView) thisView.findViewById(R.id.champMdp);
        final CheckBox remember = (CheckBox) thisView.findViewById(R.id.remember);
        baseBackground = textLogin.getBackground();

        //Code
        String email = main.getPref().getString("email",null);
        if(email != null){
            textLogin.setText(email);
            remember.setChecked(true);
        }

        //font
        buttonCo.setTypeface(fontButton);
        buttonEng.setTypeface(fontButton);
        emailField.setTypeface(fontField);
        passwdField.setTypeface(fontField);
        remember.setTypeface(fontField);

        //listeners
        buttonCo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Color
                textLogin.setBackground(baseBackground);
                textMdp.setBackground(baseBackground);

                if(textLogin.getText().toString().trim().isEmpty()){
                    textLogin.setBackgroundColor(Color.rgb(255, 102, 0));
                    return;
                }
                else if(textMdp.getText().toString().trim().isEmpty()){
                    textMdp.setBackgroundColor(Color.rgb(255, 102, 0));
                    return;
                }

                //Remember me
                if(remember.isChecked()){
                    main.getPref().edit().putString("email", textLogin.getText().toString()).commit();
                }
                else{
                    main.getPref().edit().remove("email").commit();
                }


                //Connection
                try {
                    JSONArray array = connect.connectUser(textLogin.getText().toString(), textMdp.getText().toString());
                    if(array == null){
                        Snackbar.make(thisView, "Pas de connection internet", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                    if(array.length()>0) {
                        if (((String) array.get(0)).equals("None")) {
                            Snackbar.make(thisView, "E-mail ou Mot de passe incorrect", Snackbar.LENGTH_LONG).show();
                            return;
                        }
                        for (int i = 0; i < array.length(); i++) {
                            String row = (String) array.get(i);
                            main.getListe().addData(row.trim());
                        }
                        //Show
                        main.showListe(view);
                    }
                    else{
                        Snackbar.make(thisView, "Vous n'avez aucun produit actuellement", Snackbar.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (TimeoutException e) {
                    e.printStackTrace();
                }

            }
        });

        buttonEng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Main) getActivity()).showNew(view);
            }
        });
        return thisView;
    }

    @Override
    public void onResume() {
        super.onResume();
        View actual = getView();
        if(preLogin != null){
            ((TextView) actual.findViewById(R.id.champLogin)).setText(preLogin);
            preLogin=null;
        }
        TextView textMdp = (TextView) actual.findViewById(R.id.champMdp);
        textMdp.setText("");


    }

    public void setLogin(String login){
        this.preLogin = login;
    }
}