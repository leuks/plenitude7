package classes.fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.app.plenitude7.Main;
import com.app.plenitude7.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;

import classes.java.Connect;

/**
 * Created by leuks on 18/03/2016.
 */
public class FragNewCompte extends Fragment {
    private Spinner spinJour;
    private Spinner spinMois;
    private Spinner spinAns;
    private TextView textFirstname;
    private TextView textLastname;
    private TextView textEmail;
    private TextView textPasswd;
    private CheckBox checkNewsletter;
    private CheckBox checkOffres;
    private Drawable baseBackground;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View thisView = inflater.inflate(R.layout.new_compte,container, false);
        final Main main = (Main) getActivity();
        final Connect connect = ((Main) getActivity()).getConnect();
        spinJour = (Spinner) thisView.findViewById(R.id.spinJours);
        spinMois = (Spinner) thisView.findViewById(R.id.spinMois);
        spinAns = (Spinner) thisView.findViewById(R.id.spinAns);
        final RadioGroup radiogroup = ((RadioGroup) thisView.findViewById(R.id.genderGroup));
        textFirstname = (TextView) thisView.findViewById(R.id.champFirstname);
        textLastname = (TextView) thisView.findViewById(R.id.champLastname);
        textEmail = (TextView) thisView.findViewById(R.id.champEmail);
        textPasswd = (TextView) thisView.findViewById(R.id.champPasswd);
        checkNewsletter = (CheckBox) thisView.findViewById(R.id.newsletter);
        checkOffres = (CheckBox) thisView.findViewById(R.id.offres);
        Typeface font = Typeface.createFromAsset(main.getAssets(), "fonts/Jura-DemiBold.ttf");
        final Button buttonEng = (Button) thisView.findViewById(R.id.buttonEng);
        baseBackground = textFirstname.getBackground();

        //font
        textEmail.setTypeface(font);
        textFirstname.setTypeface(font);
        textLastname.setTypeface(font);
        textPasswd.setTypeface(font);


        //bind
        ArrayList jours = new ArrayList();
        ArrayList mois = new ArrayList();
        ArrayList annees = new ArrayList();

        for(int i=1;i<=31;i++) jours.add(i);
        for(int i=1;i<=12;i++) mois.add(i);
        for(int i=1950;i<=2016;i++) annees.add(i);

        ArrayAdapter adapterJour = new ArrayAdapter(getActivity() ,R.layout.support_simple_spinner_dropdown_item,jours);
        ArrayAdapter adapterMois = new ArrayAdapter(getActivity() ,R.layout.support_simple_spinner_dropdown_item,mois);
        ArrayAdapter adapterAns = new ArrayAdapter(getActivity() ,R.layout.support_simple_spinner_dropdown_item,annees);

        spinJour.setAdapter(adapterJour);
        spinMois.setAdapter(adapterMois);
        spinAns.setAdapter(adapterAns);

        //listener
        buttonEng.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int gender = 2;
                int intNewsletter = 0;
                int intOffers = 0;

                String firstname = textFirstname.getText().toString().trim();
                String lastname = textLastname.getText().toString().trim();
                String email = textEmail.getText().toString().trim();
                String mdp = textPasswd.getText().toString().trim();
                RadioButton radioGender = (RadioButton) radiogroup.findViewById(radiogroup.getCheckedRadioButtonId());
                boolean newsletter = checkNewsletter.isChecked();
                boolean offers = checkOffres.isChecked();
                Date birth = new Date(Integer.parseInt(spinJour.getSelectedItem().toString()), Integer.parseInt(spinMois.getSelectedItem().toString()), Integer.parseInt(spinAns.getSelectedItem().toString()));

                textFirstname.setBackground(baseBackground);
                textLastname.setBackground(baseBackground);
                textEmail.setBackground(baseBackground);
                textPasswd.setBackground(baseBackground);

                //check regex
                if(firstname.isEmpty()){
                    textFirstname.setBackgroundColor(Color.rgb(255, 102, 0));
                    return;
                }
                else if(lastname.isEmpty()){
                    textLastname.setBackgroundColor(Color.rgb(255, 102, 0));
                    return;
                }
                else if(email.isEmpty()){
                    textEmail.setBackgroundColor(Color.rgb(255, 102, 0));
                    return;
                }
                else if(mdp.isEmpty()){
                    textPasswd.setBackgroundColor(Color.rgb(255, 102, 0));
                    return;
                }
                else if(!Pattern.compile(".+@+.*").matcher(email).matches()){
                    textEmail.setBackgroundColor(Color.rgb(255, 102, 0));
                    return;
                }

                if(radioGender.getId() == R.id.civiliteMal) gender = 1;
                if(newsletter) intNewsletter = 1;
                if(offers) intOffers = 1;

                try {
                    String rep = connect.createUser(gender, firstname, lastname, email, mdp, Integer.parseInt(spinJour.getSelectedItem().toString()), Integer.parseInt(spinMois.getSelectedItem().toString()), Integer.parseInt(spinAns.getSelectedItem().toString()), intNewsletter, intOffers);

                    if (rep.equals("Exists")) {
                        Snackbar.make(thisView, "Cet e-mail existe déjà", Snackbar.LENGTH_SHORT).show();
                        return;
                    }
                    else Snackbar.make(thisView, "Compte crée!", Snackbar.LENGTH_SHORT).show();

                }catch (InterruptedException e) {
                    e.printStackTrace();
                    Snackbar.make(thisView, "Problème lors de la création du compte, réesayez ultérieurement", Snackbar.LENGTH_SHORT).show();
                    return;
                } catch (ExecutionException e) {
                    e.printStackTrace();
                    Snackbar.make(thisView, "Problème lors de la création du compte, réesayez ultérieurement", Snackbar.LENGTH_SHORT).show();
                    return;
                } catch (TimeoutException e) {
                    e.printStackTrace();
                    Snackbar.make(thisView, "Problème lors de la création du compte, réesayez ultérieurement", Snackbar.LENGTH_SHORT).show();
                    return;
                }
                main.getAccueil().setLogin(email);
                main.showAccueil(thisView);
            }
        });

        return thisView;
    }

    @Override
    public void onResume() {
        super.onResume();
        View actual = getView();
        ((RadioButton) actual.findViewById(R.id.civiliteMal)).setChecked(true);
        textEmail.setText("");
        textFirstname.setText("");
        textLastname.setText("");
        textPasswd.setText("");
        checkNewsletter.setChecked(false);
        checkOffres.setChecked(false);
    }
}
