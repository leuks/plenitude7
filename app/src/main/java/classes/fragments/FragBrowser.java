package classes.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.app.plenitude7.R;

/**
 * Created by leuks on 01/04/2016.
 */
public class FragBrowser extends Fragment {
    private String plenitude = "http://www.plenitude7.com/";
    private String anti_stress = plenitude+"AS179521AZ";
    private String concentration = plenitude+"CR824795EV";
    private String relaxation = plenitude+"RT314852BY";
    private String meditation = plenitude+"MN537123CX";
    private String sommeil = plenitude+"SL780464DW";
    private String training = plenitude+"AT957036UT";
    private String confiance = plenitude+"CS469147EN";
    private String timidite = plenitude+"TE681258DM";
    private String deprime_depression = plenitude+"DP713729RS";
    private String anxiete_angoisse = plenitude+"NA835947GS";
    private String obsesite_surpoids = plenitude+"BS208342HZ";
    private String tabac = plenitude+"DT371689BC";
    private String alcool = plenitude+"DC482791LA";
    private String separation_rupture = plenitude+"SR593834RU";
    private String mentale = plenitude+"RM614771GR";
    private String pensee_positive = plenitude+"PE736899VT";
    private WebView webView;
    private String choice;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View thisView = inflater.inflate(R.layout.browser,container, false);
        webView = (WebView) thisView.findViewById(R.id.webView);

        webView.setWebViewClient(new WebViewClient());
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);

        goTo();
        return thisView;
    }

    public void goTo() {
        switch(choice){
            case FragListeApps.ANTI_STRESS:
                webView.loadUrl(anti_stress);
                break;
            case FragListeApps.CONCENTRATION:
                webView.loadUrl(concentration);
                break;
            case FragListeApps.RELAXATION:
                webView.loadUrl(relaxation);
                break;
            case FragListeApps.MEDITATION:
                webView.loadUrl(concentration);
                break;
            case FragListeApps.TRAINING:
                webView.loadUrl(training);
                break;
            case FragListeApps.SOMMEIl:
                webView.loadUrl(sommeil);
                break;
            case FragListeApps.CONFIANCE:
                webView.loadUrl(confiance);
                break;
            case FragListeApps.TIMIDITE:
                webView.loadUrl(timidite);
                break;
            case FragListeApps.DEPRIME:
                webView.loadUrl(deprime_depression);
                break;
            case FragListeApps.ANXIETE:
                webView.loadUrl(anxiete_angoisse);
                break;
            case FragListeApps.OBESITE:
                webView.loadUrl(obsesite_surpoids);
                break;
            case FragListeApps.TABAC:
                webView.loadUrl(tabac);
                break;
            case FragListeApps.ALCOOL:
                webView.loadUrl(alcool);
                break;
            case FragListeApps.RUPTURE:
                webView.loadUrl(separation_rupture);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Toast.makeText(getActivity().getBaseContext(), "Orientez votre appareil en paysage pour un meilleur rendu", Toast.LENGTH_SHORT
        ).show();
    }

    public void setChoice(String choice){
        this.choice=choice;
    }
}
