package classes.java;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by leuks on 06/04/2016.
 */
public class ThreadNetworkConnect extends AsyncTask<String, String, Void> {
    private String login;
    private String mdp;
    private JSONArray arrayJS;

    public ThreadNetworkConnect(String login, String mdp){
        this.login=login;
        this.mdp=mdp;
    }

    protected Void doInBackground(String... var1) {
        HttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
        HttpResponse response;
        JSONObject json = new JSONObject();

        try {
            HttpPost post = new HttpPost("http://www.plenitude7.com/wa_ps_1_5_2_0/checkUser.php");
            json.put("from", "app");
            json.put("email", login);
            json.put("mdp", mdp);
            StringEntity se = new StringEntity(json.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);


                    /*Checking response */
            if (response != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                StringBuilder builder = new StringBuilder();
                for (String line = null; (line = reader.readLine()) != null;) {
                    builder.append(line).append("\n");
                }
                JSONTokener tokener = new JSONTokener(builder.toString());
                 arrayJS = new JSONArray(tokener);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(String stirng) {
        // TODO: check this.exception
        // TODO: do something with the feed
    }

    public JSONArray getArrayJS(){
        return arrayJS;
    }
}