package classes.java;

import com.app.plenitude7.Main;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by leuks on 18/03/2016.
 */
public class Connect {
    private Main main;

    public Connect(Main main){
        this.main=main;
    }

    public JSONArray connectUser(String login, String mdp) throws JSONException, InterruptedException, ExecutionException, TimeoutException {
        ThreadNetworkConnect thread = new ThreadNetworkConnect(login, mdp);
        thread.execute("go");
        thread.get(5000, TimeUnit.MILLISECONDS);
        return thread.getArrayJS();
    }

    public String createUser(int gender, String firstName, String lastName, String email, String mdp, int day, int month, int year, int newsletter, int offres) throws InterruptedException, ExecutionException, TimeoutException {
        ThreadNetworkRegister thread = new ThreadNetworkRegister(gender, firstName, lastName, email, mdp, day, month, year, newsletter, offres);
        thread.execute("go");
        thread.get(10000, TimeUnit.MILLISECONDS);
        return thread.getRep();
    }

}
