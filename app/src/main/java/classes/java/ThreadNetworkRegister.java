package classes.java;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by leuks on 26/04/2016.
 */
public class ThreadNetworkRegister extends AsyncTask<String, String, Void> {
    private int gender;
    private String fistname;
    private String lastname;
    private String email;
    private String mdp;
    private int day;
    private int month;
    private int year;
    private int newsletter;
    private int offers;
    private String rep;

    public ThreadNetworkRegister(int gender, String fistname, String lastname,String email, String mdp, int day, int month, int year, int newsletter, int offers){
        this.gender=gender;
        this.fistname=fistname;
        this.lastname=lastname;
        this.email=email;
        this.mdp=mdp;
        this.day=day;
        this.month=month;
        this.year=year;
        this.newsletter=newsletter;
        this.offers=offers;
    }

    protected Void doInBackground(String... var1) {
        HttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); //Timeout Limit
        HttpResponse response;
        JSONObject json = new JSONObject();

        try {
            HttpPost post = new HttpPost("http://www.plenitude7.com/wa_ps_1_5_2_0/createUser.php");
            json.put("email", email);
            json.put("firstname", fistname);
            json.put("lastname", lastname);
            json.put("mdp", mdp);
            json.put("day", day);
            json.put("month", month);
            json.put("year", year);
            json.put("newsletter", newsletter);
            json.put("offers", offers);
            json.put("gender", gender);
            StringEntity se = new StringEntity(json.toString());
            se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            post.setEntity(se);
            response = client.execute(post);

            /*Checking response */
            if (response != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
                StringBuilder builder = new StringBuilder();
                for (String line = null; (line = reader.readLine()) != null;) {
                    builder.append(line).append("\n");
                }
                rep=builder.toString().trim();
                System.out.println("---------"+rep+"----");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    protected void onPostExecute(String stirng) {
        // TODO: check this.exception
        // TODO: do something with the feed
    }

    public String getRep(){
        return rep;
    }
}
